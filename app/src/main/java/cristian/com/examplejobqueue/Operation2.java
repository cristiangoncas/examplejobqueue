package cristian.com.examplejobqueue;

import org.jdeferred.Promise;

public class Operation2 extends Operation {

    @Override
    Promise<Object, Throwable, Integer> doInBackground() {
        work = new Runnable() {
            @Override
            public void run() {
                try {
                    int count = 0;
                    deferredObject.notify(0);
                    for (int i = 0; i <= 100; i += 20) {
                        Thread.sleep(5000);
                        deferredObject.notify(i);
                        count = i;
                    }
                    deferredObject.notify(count);
                } catch (InterruptedException e) {

                }
                deferredObject.resolve(new User());
            }
        };
        mDefManager.when(work);
        return mDefManager.when(deferredObject);
    }
}
