package cristian.com.examplejobqueue;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.jdeferred.Promise;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity implements OperationManager.OperationCallback {

    public TextView txt1, txt2, txt3;
    private static final int TYPE_DEFAULT = 1;
    private static final int TYPE_USER = 2;
    private static final int TYPE_PROMO = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final OperationManager om = new OperationManager();

        txt1 = (TextView) findViewById(R.id.threadfield1);

        Button btn1 = (Button) findViewById(R.id.thread1);

        final WeakReference<OperationManager.OperationCallback> weakReferer = new WeakReference<OperationManager.OperationCallback>(this);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                om.executeOperation(new Operation1(), weakReferer, TYPE_DEFAULT);
            }
        });

        txt2 = (TextView) findViewById(R.id.threadfield2);

        Button btn2 = (Button) findViewById(R.id.thread2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                om.executeOperation(new Operation2(), weakReferer, TYPE_USER);
            }
        });

        txt3 = (TextView) findViewById(R.id.threadfield3);

        Button btn3 = (Button) findViewById(R.id.thread3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                om.executeOperation(new Operation3(), weakReferer, TYPE_PROMO);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Test", "Pause");
    }

    private void modifyTxt(int i, String msg) {
        switch (i) {
            case 1:
                txt1.setText(msg);
                break;
            case 2:
                txt2.setText(msg);
                break;
            case 3:
                txt3.setText(msg);
                break;
        }
    }

    @Override
    public void operationDone(int type, Object result) {
        switch (type) {
            case TYPE_DEFAULT:
                Toast.makeText(getApplicationContext(), (String) result, Toast.LENGTH_SHORT).show();
                break;
            case TYPE_USER:
                User user = (User) result;
                Toast.makeText(getApplicationContext(), user.user, Toast.LENGTH_SHORT).show();
                break;
            case TYPE_PROMO:
                Promo promo = (Promo) result;
                Toast.makeText(getApplicationContext(), promo.promo, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void operationFail(int type, Object result) {

    }

    @Override
    public void operationProgress(int type, Object result) {
        switch (type) {
            case TYPE_DEFAULT:
                modifyTxt(1, String.valueOf((int) result));
                break;
            case TYPE_USER:
                modifyTxt(2, String.valueOf((int) result));
                break;
            case TYPE_PROMO:
                modifyTxt(3, String.valueOf((int) result));
                break;
        }
    }

    @Override
    public void operationAlways(int type, Promise.State state, Object result, Object rejected) {

    }
}
