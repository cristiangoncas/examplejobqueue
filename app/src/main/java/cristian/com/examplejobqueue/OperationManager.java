package cristian.com.examplejobqueue;

import android.util.Log;

import org.jdeferred.AlwaysCallback;
import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.ProgressCallback;
import org.jdeferred.Promise;

import java.lang.ref.WeakReference;

public class OperationManager {

    public interface OperationCallback {
        void operationDone(int type, Object result);

        void operationFail(int type, Object result);

        void operationProgress(int type, Object result);

        void operationAlways(int type, Promise.State state, Object result, Object rejected);
    }

    public void executeOperation(final Operation operation, final WeakReference<OperationCallback> reference, final int type) {

        operation.doInBackground().then(new DoneCallback<Object>() {
            @Override
            public void onDone(Object result) {
                if (reference.get() != null) {
                    reference.get().operationDone(type, result);
                }
            }
        }).fail(new FailCallback<Throwable>() {
            @Override
            public void onFail(Throwable result) {
                if (reference.get() != null) {
                    reference.get().operationFail(type, result);
                    Log.d("Test", "Fail: " + result.getMessage());
                }
            }
        }).progress(new ProgressCallback<Integer>() {
            @Override
            public void onProgress(Integer progress) {
                if (reference.get() != null) {
                    reference.get().operationProgress(type, progress);
                    Log.d("Test", "Progress: " + progress + "%");
                }
            }
        }).always(new AlwaysCallback() {
                      @Override
                      public void onAlways(Promise.State state, Object resolved, Object rejected) {
                          if (reference.get() != null) {
                              reference.get().operationAlways(type, state, resolved, rejected);
                          }
                      }
                  }

        );
    }
}