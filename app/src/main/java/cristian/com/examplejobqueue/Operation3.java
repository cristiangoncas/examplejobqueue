package cristian.com.examplejobqueue;

import org.jdeferred.Promise;

public class Operation3 extends Operation {

    @Override
    Promise<Object, Throwable, Integer> doInBackground() {
        work = new Runnable() {
            @Override
            public void run() {
                try {
                    int count = 0;
                    deferredObject.notify(0);
                    for (int i = 0; i <= 100; i += 25) {
                        Thread.sleep(4000);
                        deferredObject.notify(i);
                        count = i;
                    }
                    deferredObject.notify(count);
                } catch (InterruptedException e) {

                }
                deferredObject.resolve(new Promo());
            }
        };
        mDefManager.when(work);
        return mDefManager.when(deferredObject);
    }
}
