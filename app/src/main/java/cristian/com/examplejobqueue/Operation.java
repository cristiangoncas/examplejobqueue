package cristian.com.examplejobqueue;

import android.util.Log;

import org.jdeferred.Promise;
import org.jdeferred.android.AndroidDeferredManager;
import org.jdeferred.impl.DeferredObject;

import java.util.concurrent.Executors;

public abstract class Operation {

    protected AndroidDeferredManager mDefManager = new AndroidDeferredManager(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()));
    protected final DeferredObject<Object, Throwable, Integer> deferredObject = new DeferredObject<Object, Throwable, Integer>();
    protected Runnable work;

    Promise<Object, Throwable, Integer> doInBackground() {
        work = new Runnable() {
            @Override
            public void run() {
                try {
                    deferredObject.resolve(new Object());
                } catch (Throwable ex) {
                    deferredObject.reject(ex);
                }
            }
        };
        mDefManager.when(work);
        return mDefManager.when(deferredObject);
    }
}
